package com.company;

import java.util.ArrayList;
import java.util.Random;

public class Filter {
    public static int[] randomArray(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
    public static String[] randomArray(String[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
    public static char[] randomArray(char[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
    public static boolean[] randomArray(boolean[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }
    public static float[] randomArray(float[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }

    public static double[] randomArray(double[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        return array;
    }




    public static boolean isEmpty(int[] array){
        return (array.length==0) ;
    }
    public static boolean isEmpty(char[] array){
        return (array.length==0) ;
    }
    public static boolean isEmpty(String[] array){
        return (array.length==0) ;
    }
    public static boolean isEmpty(boolean[] array){
        return (array.length==0) ;
    }
    public static boolean isEmpty(float[] array){
        return (array.length==0) ;
    }

    public static boolean isEmpty(double[] array){return (array.length==0) ;}









    public static int[] randomArray(int[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }

    public static String[] randomArray(String[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }
    public static char[] randomArray(char[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }
    public static boolean[] randomArray(boolean[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }
    public static float[] randomArray(float[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }

    public static double[] randomArray(double[] array,int limit) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(limit);
        }
        return array;
    }



    public static void printArray(int[] array) {
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }

    public static void printArray(char[] array) {
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }
    public static void printArray(String[] array) {
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }


    public static void printArray(float[] array) {
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }


    public static void printArray(double[] array) {
        System.out.print("[ ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println("]");
    }







    //ищет то число в масиве которое мы передали если оно есть то возрощяет tru eсли нет то false
    public static boolean checkNumber(int a, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return true;
            }
        }
        return false;
    }
//ищем в главном масиве повторяищие елементы если они есть то выводим их
    public static int[] getWithFilter(int[] array, int... a) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        boolean isCheck = true;
        for (int i = 0; i < array.length; i++) {
            isCheck = true;
            for (int j = 0; j < a.length; j++) {
                if (array[i] == a[j]) {
                    isCheck = false;
                    break;
                }
            }
            if (isCheck) arrayList.add(array[i]);
        }
        int[] result = new int[arrayList.size()];

        for (int i = 0; i < result.length; i++)
            result[i] = arrayList.get(i);

        return result;
    }
//ищем в главном масиве повторяищие елементы если они есть то выводим их

    public static String[] getWithFilter(String[] array, String... a) {
        ArrayList<String> arrayList = new ArrayList<>();
        boolean isCheck = true;
        for (int i = 0; i < array.length; i++) {
            isCheck = true;
            for (int j = 0; j < a.length; j++) {
                if (array[i] == a[j]) {
                    isCheck = false;
                    break;
                }
            }
            if (isCheck) arrayList.add(array[i]);
        }
        String[] result = new String[arrayList.size()];

        for (int i = 0; i < result.length; i++)
            result[i] = arrayList.get(i);

        return result;
    }
//ищем в главном масиве повторяищие елементы если они есть то выводим их

    public static char[] getWithFilter(char[] array, char... a) {
        ArrayList<Character> arrayList = new ArrayList<>();
        boolean isCheck = true;
        for (int i = 0; i < array.length; i++) {
            isCheck = true;
            for (int j = 0; j < a.length; j++) {
                if (array[i] == a[j]) {
                    isCheck = false;
                    break;
                }
            }
            if (isCheck) arrayList.add(array[i]);
        }
        char[] result = new char[arrayList.size()];

        for (int i = 0; i < result.length;i++)
            result[i] = arrayList.get(i);

        return result;
    }
//ищем в главном масиве повторяищие елементы если они есть то выводим их

    public static float[] getWithFilter(float[] array, float... a) {
        ArrayList<Float> arrayList = new ArrayList<>();
        boolean isCheck = true;
        for (int i = 0; i < array.length; i++) {
            isCheck = true;
            for (int j = 0; j < a.length; j++) {
                if (array[i] == a[j]) {
                    isCheck = false;
                    break;
                }
            }
            if (isCheck) arrayList.add(array[i]);
        }
        float[] result = new float[arrayList.size()];

        for (int i = 0; i < result.length; i++)
            result[i] = arrayList.get(i);

        return result;
    }
//ищем в главном масиве повторяищие елементы если они есть то выводим их

    public static double[] getWithFilter(double[] array, double... a) {
        ArrayList<Double> arrayList = new ArrayList<>();
        boolean isCheck = true;
        for (int i = 0; i < array.length; i++) {
            isCheck = true;
            for (int j = 0; j < a.length; j++) {
                if (array[i] == a[j]) {
                    isCheck = false;
                    break;
                }
            }
            if (isCheck) arrayList.add(array[i]);
        }
        double[] result = new double[arrayList.size()];

        for (int i = 0; i < result.length; i++)
            result[i] = arrayList.get(i);

        return result;
    }





//если в нашем масиве встретилась то число которое мы передали то тозвращяем индекс того числа в масиве если нет то -1
    public static int indexOf(int a, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return i;
            }
        }
        return -1;
    }
//если в нашем масиве встретилась то строку которое мы передали то звращяем индекс того числа в масиве если нет то -1

    public static int indexOf(String a, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return i;
            }
        }
        return -1;
    }
//если в нашем масиве встретилась тот символ которое мы передали то звращяем индекс того числа в масиве если нет то -1

    public static int indexOf(char a, char[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return i;
            }
        }
        return -1;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс того числа в масиве если нет то -1

    public static int indexOf(float a, float[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return i;
            }
        }
        return -1;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс того числа в масиве если нет то -1

    public static int indexOf(double a, double[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                return i;
            }
        }
        return -1;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс
    public static int indexOfLast(int a, int[] array) {
        int ff = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                ff = i;
            }
        }
        return ff;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс

    public static int indexOfLast(char a, char[] array) {
        int ff = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                ff = i;
            }
        }
        return ff;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс

    public static int indexOfLast(String a, String[] array) {
        int ff = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                ff = i;
            }
        }
        return ff;
    }
//если в нашем масиве встретилась то число которое мы передали то звращяем индекс

    public static int indexOfLast(float a, float[] array) {
        int ff = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                ff = i;
            }
        }
        return ff;
    }

    public static int indexOfLast(double a, double[] array) {
        int ff = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == a) {
                ff = i;
            }
        }
        return ff;
    }
//
    public static int[] setUnique(int[] array) {
        ArrayList<Integer> arrayStart = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (arrayStart.indexOf(array[i]) == -1) arrayStart.add(array[i]);
        }

        int[] arrayResult = new int[arrayStart.size()];

        for (int i = 0; i < arrayStart.size(); i++)
            arrayResult[i] = arrayStart.get(i);

        return arrayResult;
    }

    public static String[] setUnique(String[] array) {
        ArrayList<String> arrayStart = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (arrayStart.indexOf(array[i]) == -1) arrayStart.add(array[i]);
        }

        String[] arrayResult = new String[arrayStart.size()];

        for (int i = 0; i < arrayStart.size(); i++) {
            arrayResult[i] = arrayStart.get(i);
        }
        return arrayResult;
    }

    public static char[] setUnique(char[] array) {
        ArrayList<Character> arrayStart = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (arrayStart.indexOf(array[i]) == -1) arrayStart.add(array[i]);
        }
        char[] arrayResult = new char[arrayStart.size()];

        for (int i = 0; i < arrayStart.size(); i++)
            arrayResult[i] = arrayStart.get(i);

        return arrayResult;
    }

    public static float[] setUnique(float[] array) {
        ArrayList<Float> arrayStart = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (arrayStart.indexOf(array[i]) == -1) arrayStart.add(array[i]);
        }

        float[] arrayResult = new float[arrayStart.size()];

        for (int i = 0; i < arrayStart.size(); i++)
            arrayResult[i] = arrayStart.get(i);

        return arrayResult;
    }



    public static double[] setUnique(double[] array) {
        ArrayList<Double> arrayStart = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            if (arrayStart.indexOf(array[i]) == -1) arrayStart.add(array[i]);
        }

        double[] arrayResult = new double[arrayStart.size()];

        for (int i = 0; i < arrayStart.size(); i++)
            arrayResult[i] = arrayStart.get(i);

        return arrayResult;
    }





    public static int[] merger(int[]... array) {
        int sum = 0;
        int index = 0;
        for (int ic = 0; ic < array.length; ic++) {
            sum += array[ic].length;
        }
        int[] a = new int[sum];

        for (int i = 0; i < array.length; i++) {
            for (int g = 0; g < array[i].length; g++) {
                a[index] = array[i][g];
                index++;
            }
        }
        return a;
    }


    public static String[] merger(String[]... array) {
        int sum = 0;
        int index = 0;
        for (int ic = 0; ic < array.length; ic++) {
            sum += array[ic].length;
        }
        String[] a = new String[sum];

        for (int i = 0; i < array.length; i++) {
            for (int g = 0; g < array[i].length; g++) {

                a[index] = array[i][g];
                index++;

            }
        }
        return a;

    }


    public static char[] merger(char[]... array) {
        int sum = 0;
        int index = 0;
        for (int ic = 0; ic < array.length; ic++) {
            sum += array[ic].length;
        }
        char[] a = new char[sum];

        for (int i = 0; i < array.length; i++) {
            for (int g = 0; g < array[i].length; g++) {

                a[index] = array[i][g];
                index++;

            }
        }
        return a;

    }

    public static float[] merger(float[]... array) {
        int sum = 0;
        int index = 0;
        for (int ic = 0; ic < array.length; ic++) {
            sum += array[ic].length;
        }
        float[] a = new float[sum];

        for (int i = 0; i < array.length; i++) {
            for (int g = 0; g < array[i].length; g++) {

                a[index] = array[i][g];
                index++;

            }
        }
        return a;

    }

    public static double[] merger(double[]... array) {
        int sum = 0;
        int index = 0;
        for (int ic = 0; ic < array.length; ic++) {
            sum += array[ic].length;
        }
        double[] a = new double[sum];

        for (int i = 0; i < array.length; i++) {
            for (int g = 0; g < array[i].length; g++) {

                a[index] = array[i][g];
                index++;
            }
        }
        return a;
    }
}
